package service;

import app.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class CalculatorService {

    public static final String EQUAL_SIGN = "=";
    public static final String PLUS_SIGN = "+";
    public static final String MINUS_SIGN = "-";
    public static final String MULTIPLY_SIGN = "*";
    public static final String DIVIDE_SIGN = "/";

    //calculations are based on shift-reduce parsing method
    public static Double calculate(String expression) {

        //two stacks for shift-reduce parsing - with operands and operators
        Stack<Double> numbers = new Stack<>();
        Stack<Operator> operators = new Stack<>();

        //list of tokens in expression
        List<String> tokens = tokenizeInput(expression);
        Double result = (double) 0;

        for (String token : tokens) {
            if (isOperator(token)) {
                Operator oper = Operator.of(token);

                if (operators.isEmpty()) {
                    operators.push(oper);

                } else {
                    assert oper != null;
                    if (oper.getPriority() < operators.peek().getPriority()) {
                        operators.push(oper);

                    } else if (oper.getPriority() > operators.peek().getPriority()) {
                        result = operators.pop().apply(numbers.pop(), numbers.pop());
                        numbers.push(result);
                        operators.push(oper);

                    } else if (oper.getPriority().equals(operators.peek().getPriority())) {
                        result = oper.apply(numbers.pop(), numbers.pop());
                        numbers.push(result);
                        operators.pop();
                    }
                }

            } else {

                numbers.push(Double.parseDouble(token));
            }
        }

        //executing calculations on both stacks
        while (!operators.isEmpty()) {
            Operator oper = operators.peek();
            Double number1 = numbers.pop();
            Double number2 = numbers.pop();
            result = oper.apply(number2, number1);
            numbers.push(result);
            operators.pop();
        }

        return result;
    }

    private static List<String> tokenizeInput(String input) {
        List<String> tokens = new ArrayList<>();

        StringTokenizer tokenizer = new StringTokenizer(input, "*/+-", true);
        while (tokenizer.hasMoreElements()) {
            tokens.add(tokenizer.nextToken());
        }

        return tokens;
    }

    private static boolean isOperator(String input) {
        return input.equals(EQUAL_SIGN) || input.equals(MINUS_SIGN) || input.equals(MULTIPLY_SIGN) || input.equals(DIVIDE_SIGN);
    }
}
