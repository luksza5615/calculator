package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import service.CalculatorService;


public class Controller {
    @FXML
    private TextField input;

    @FXML
    void putToInput(ActionEvent event) {

        String userInput = ((Button) event.getSource()).getText();

        if (!userInput.equals(CalculatorService.EQUAL_SIGN)) {
            input.appendText(userInput);
        } else {
            String expressionToCalculate = input.getText();
            Double result = calculate(expressionToCalculate);
            input.setText(String.valueOf(result));
        }
    }

    @FXML
    Double calculate(String expression) {
        return CalculatorService.calculate(expression);
    }

    @FXML
    void clear(ActionEvent event) {
        input.clear();
    }

}
