package app;

import service.CalculatorService;

import java.util.function.BiFunction;

public enum Operator implements BiFunction<Double, Double, Double> {

    PLUS(((a, b) -> a + b), 2),
    MINUS(((a, b) -> a - b), 2),
    MULTIPLY(((a, b) -> a * b), 1),
    DIVIDE(((a, b) -> a / b), 1);

    private final BiFunction<Double, Double, Double> function;
    private Integer priority;

    @Override
    public Double apply(Double o, Double o2) {
        return function.apply(o, o2);
    }

    public static Operator of(String s) {
        if (s.equals(CalculatorService.PLUS_SIGN)) {
            return PLUS;
        } else if (s.equals(CalculatorService.MINUS_SIGN)) {
            return MINUS;
        } else if (s.equals(CalculatorService.MULTIPLY_SIGN)) {
            return MULTIPLY;
        } else if (s.equals(CalculatorService.DIVIDE_SIGN)) {
            return DIVIDE;
        } else {
            return null;
        }
    }

    Operator(BiFunction<Double, Double, Double> function, Integer priority) {
        this.function = function;
        this.priority = priority;
    }

    public Integer getPriority() {
        return this.priority;
    }

}
