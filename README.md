# Overview
Simple calculator with basic calculations.

## Implementation
* JavaFX
* functional interface
* shift-reduce parsing method

![](view.png)